import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from './Pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import DetailSewa from './Pages/DetailSewa';
import Formkategori from './Pages/Formkategori';
import HasilPencarian from './Pages/HasilPencarian';


const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/PencarianMobil' element={<Formkategori />} />
        <Route path='/hasilPencarian' element={<HasilPencarian />} />
        <Route path='/detailSewa' element={<DetailSewa />} />
      </Routes>

    </BrowserRouter>
  )
}

export default App