import React from "react";
import Footer from "../Components/Footer/Footer";
import NavbarComp from "../Components/NavbarComp/NavbarComp";
import DetailSewa from "../Components/DetailSewaMobil/DetailSewa";

const DetailSewaPaketMobil = () => {
  return (
    <div>
      <NavbarComp />
      <DetailSewa />
      <Footer />
    </div>
  );
};

export default DetailSewaPaketMobil;
