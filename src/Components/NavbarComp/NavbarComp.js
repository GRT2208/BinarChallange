import React from "react";
import "./navbar.css";

const Navbar = () => {
  return (
    <div className="navbar navbar-expand-lg fixed-top">
      <div className="container">
        <img src="./assets/images/Carbrand.png" id="brandLogo" href="/Home"></img>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggler="offcanfas"
          data-bs-target="#offcanvasNavbar"
          aria-controls="offcanvasNavbar"
        >
          <a className="navbar-toggler-icon"></a>
        </button>
        <div
          className="offcanvas offcanvas-end"
          tabIndex="-1"
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
        >
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
              BINAR
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>
          <div className="offcanvas-body">
            <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
              <li className="nav-item">
                <a className="nav-link" href="/#ServicesComp">
                  Our Services
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#Whyus">
                  Why US
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#TestimonialComp">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#Faq">
                  FAQ
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
